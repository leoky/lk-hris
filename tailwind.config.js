/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./src/**/*.{html,ts}",
  ],
  theme: {
    extend: {
      colors: {
        'primary': {
          DEFAULT: '#005cbb',
        },
        'danger': {
          DEFAULT: '#ef4444',
        }
      },
      spacing: {
        'container': '24px'
      }
    },
  },
  plugins: [],
}

