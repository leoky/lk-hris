export const dummyGroup = [
    {
        "label": "Training",
        "value": "training"
    },
    {
        "label": "Human Resources",
        "value": "human resources"
    },
    {
        "label": "Accounting",
        "value": "accounting"
    },
    {
        "label": "Business Development",
        "value": "business development"
    },
    {
        "label": "Legal",
        "value": "legal"
    },
    {
        "label": "Product Management",
        "value": "product management"
    },
    {
        "label": "Support",
        "value": "support"
    },
    {
        "label": "Marketing",
        "value": "marketing"
    },
    {
        "label": "Research and Development",
        "value": "research and development"
    },
    {
        "label": "Services",
        "value": "services"
    },
    {
        "label": "Engineering",
        "value": "engineering"
    },
    {
        "label": "Sales",
        "value": "sales"
    }
];