import { Directive, HostListener } from '@angular/core';

@Directive({
  selector: '[lkNumberOnly]',
  standalone: true
})
export class NumberOnlyDirective {

  constructor() { }

  @HostListener('keydown', ['$event'])
  onKeyDown(e: KeyboardEvent) {
    if (e.key === '.' ||
      e.key === 'e' ||
      e.key === 'E' ||
      e.key === '+' ||
      e.key === '-'
    ) {
      e.preventDefault();
    }
  }

}
