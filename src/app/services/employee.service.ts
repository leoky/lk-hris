import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Employee } from '@models/employee';
import { TableFilter } from '@models/table';
import { Observable, from, lastValueFrom, map, tap } from 'rxjs';
import { ConfirmationDialogService } from './confirmation-dialog.service';

const defaultFilter: TableFilter = {
  pageIndex: 0,
  pageSize: 25,
  sortActive: '',
  sortDirection: '',
  search: '{}',
}
@Injectable({
  providedIn: 'root'
})
export class EmployeeService {

  // simulate fake server
  storedEmployees: Employee[] = [];
  // end

  tableFilter: TableFilter = { ...defaultFilter }

  constructor(
    private http: HttpClient,
    private snackbar: MatSnackBar,
    private confirmationDialogService: ConfirmationDialogService,
  ) { }

  fetchEmployees() {
    return this.http.get<Employee[]>('/dummy/employee-dummy.json').pipe(
      map(data => {
        // simulate: return data stored in memory instead dummy if have any value
        if (this.storedEmployees.length) {
          return this.storedEmployees;
        }
        return data;
      }),
      tap(data => {
        // simulate: after we fetch we store data in memory; only for empty storedEmployees
        if (data.length && this.storedEmployees.length === 0) {
          this.storedEmployees = data;
        }
      })
    );
  }

  addEmployee(data: Employee) {
    return new Observable(subs => {
      // simulate: to add in memory
      data.id = Date.now();
      this.storedEmployees.unshift(data);
      setTimeout(() => {
        subs.next(true);
        subs.complete();
      }, 500);
    });
  }

  updateEmployee(data: Employee, id: number) {
    return new Observable(subs => {
      // simulate: update in memory
      setTimeout(() => {
        const index = this.storedEmployees.findIndex(s => s.id === id);
        if (index >= 0) {
          data.id = id;
          this.storedEmployees[index] = data;
          subs.next(true);
        } else {
          this.snackbar.open('Employee not found', 'close');
          subs.error('Employee not found');
        }
        subs.complete();
      }, 300);
    });
  }

  fetchEmployeeDetail(id: number) {
    return from(new Promise(async (resolve, reject) => {
      try {
        // simulate: if page hard refresh it need to get list data first
        if (this.storedEmployees.length === 0) {
          await lastValueFrom(this.fetchEmployees());
        }
        setTimeout(() => {
          const employee = this.storedEmployees.find(e => e.id === id);
          if (employee) {
            resolve(employee);
          } else {
            this.snackbar.open('Employee not found', 'close');
            resolve(null);
          }
        }, 300);
      } catch (error) {
        reject(error);
      }
    }));
  }

  deleteEmployee(id: number) {
    return new Observable(subs => {
      const index = this.storedEmployees.findIndex(s => s.id === id);
      setTimeout(() => {
        if (index >= 0) {
          this.storedEmployees.splice(index, 1);
          subs.next(true);
        } else {
          this.snackbar.open('Employee not found', 'close');
          subs.error('Employee not found');
        }
        subs.complete();
      }, 300);
    });
  }
  async deleteEmployeeWithConfirmation(id: number, email: string) {
    const dialogRef = this.confirmationDialogService.deleteEmployee(email);

    const dialogRes = await lastValueFrom(dialogRef.afterClosed());

    if (dialogRes) {
      await lastValueFrom(this.deleteEmployee(id));
      return true;
    }
    return false;
  }

  // handle filter 
  get filter() {
    return this.tableFilter;
  }

  updateFilter(param: TableFilter) {
    this.tableFilter = param;
  }

  resetFilter() {
    this.tableFilter = { ...defaultFilter };
  }
}
