import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ConfirmationDialogComponent } from '@components/dialog/confirmation-dialog/confirmation-dialog.component';
import { ConfirmationDialogData } from '@models/dialog';

@Injectable({
  providedIn: 'root'
})
export class ConfirmationDialogService {

  constructor(
    private matDialog: MatDialog,
  ) { }

  deleteEmployee(name: string,) {
    const data: ConfirmationDialogData = {
      header: 'Delete Employee',
      message: `Are you sure want to delete this employee (${name})`,
      submitText: 'Delete',
    }
    const dialogRef = this.matDialog.open(ConfirmationDialogComponent, {
      data,
    });

    return dialogRef;

  }
}
