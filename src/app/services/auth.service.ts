import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { BehaviorSubject, Observable } from 'rxjs';
import { LS_TOKEN } from '../constants/ls';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private isLoginBS = new BehaviorSubject<boolean>(false);
  isLogin$ = this.isLoginBS.asObservable();

  constructor(
    private _snackBar: MatSnackBar
  ) {
    this.checkToken();
  }

  checkToken() {
    const ls = localStorage.getItem(LS_TOKEN);
    this.isLoginBS.next(ls ? true : false);
  }

  login(email: string, password: string) {
    // fake call API
    return new Observable((subscriber) => {
      setTimeout(() => {
        if (email === 'hris@lk.com' && password === 'leoleoleo') {
          localStorage.setItem(LS_TOKEN, email + password);
          this.isLoginBS.next(true);
          subscriber.next(true);
        } else {
          this.isLoginBS.next(false);
          this._snackBar.open('Invalid email or password', 'Close', { duration: 2000 });
          subscriber.error({ message: 'Invalid email or message' });
        }
        subscriber.complete();
      }, 500);
    });
  }

  logout() {
    this.isLoginBS.next(false);
    localStorage.clear();
    window.location.href = '/login';
  }
}
