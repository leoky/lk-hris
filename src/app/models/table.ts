export interface TableFilter {
    pageSize: number;
    pageIndex: number;
    sortActive: string;
    sortDirection: 'asc' | 'desc' | '';
    search: string;
}