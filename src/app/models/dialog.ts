export interface ConfirmationDialogData<T = any> {
    header?: string;
    message?: string;
    cancelText?: string;
    submitText?: string;
    data?: T
}