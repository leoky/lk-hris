import { NgModule } from '@angular/core';
import { InputComponent } from './input.directive';
import { LabelDirective } from './label.directive';

@NgModule({
  declarations: [],
  imports: [
    InputComponent,
    LabelDirective,
  ],
  exports: [
    InputComponent,
    LabelDirective,
  ]
})
export class InputModule { }
