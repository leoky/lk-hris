import { Directive } from '@angular/core';

@Directive({
  selector: '[lkLabel]',
  standalone: true,
  host: {
    'class': 'block flex flex-col gap-1'
  }
})
export class LabelDirective {

  constructor() { }

}
