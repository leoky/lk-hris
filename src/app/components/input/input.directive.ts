import { Directive } from '@angular/core';

@Directive({
  selector: '[lkInput]',
  standalone: true,
  host: {
    'class': `lk-input px-4 py-2 border border-slate-300 rounded-lg min-h-11 
              focus:outline-primary 
              disabled:bg-gray-100 disabled:cursor-not-allowed 
              focus:invalid:outline-danger invalid:border-danger`
  }
})
export class InputComponent {

  constructor() { }

}
