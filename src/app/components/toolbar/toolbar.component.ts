import { ChangeDetectionStrategy, Component, EventEmitter, Output } from '@angular/core';
import { MatDividerModule } from '@angular/material/divider';
import { MatIconModule } from '@angular/material/icon';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatMenuModule } from '@angular/material/menu';
import { AuthService } from '@services/auth.service';

@Component({
  selector: 'lk-toolbar',
  standalone: true,
  imports: [MatToolbarModule, MatDividerModule, MatIconModule, MatButtonModule, MatMenuModule],
  templateUrl: './toolbar.component.html',
  styleUrl: './toolbar.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ToolbarComponent {
  @Output() menuClick = new EventEmitter<boolean>();

  constructor(
    private authService: AuthService,
  ) { }

  logout() {
    this.authService.logout();
  }
}
