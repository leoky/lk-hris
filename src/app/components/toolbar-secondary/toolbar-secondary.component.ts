import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';

@Component({
  selector: 'lk-toolbar-secondary',
  standalone: true,
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [
    MatButtonModule, MatIconModule,
  ],
  templateUrl: './toolbar-secondary.component.html',
  styles: ``
})
export class ToolbarSecondaryComponent {
  @Input() enableBackButton = false;
  @Output() backButtonClick = new EventEmitter<Event>();

}
