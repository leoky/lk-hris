import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
  selector: 'lk-card',
  standalone: true,
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [],
  template: `<ng-content/>`,
  styles: ``,
  host: {
    'class': 'block bg-white border rounded-lg'
  }
})
export class CardComponent {

}

@Component({
  selector: 'lk-card-content',
  standalone: true,
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [],
  template: `<ng-content/>`,
  styles: ``,
  host: {
    'class': 'block p-container'
  }
})
export class CardContentComponent {

}
