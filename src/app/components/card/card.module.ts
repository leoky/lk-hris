import { NgModule } from '@angular/core';
import { CardComponent, CardContentComponent } from './card.component';


@NgModule({
  declarations: [],
  imports: [
    CardComponent,
    CardContentComponent,
  ],
  exports: [
    CardComponent,
    CardContentComponent,
  ]
})
export class CardModule { }
