import { Component } from '@angular/core';
import { FormBuilder, ReactiveFormsModule, Validators } from '@angular/forms';
import { MatCardModule } from '@angular/material/card';
import { MatButtonModule } from '@angular/material/button';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { AuthService } from '@services/auth.service';
import { lastValueFrom } from 'rxjs';
import { Router } from '@angular/router';
import { InputModule } from '@components/input/input.module';

@Component({
  selector: 'lk-login',
  standalone: true,
  imports: [ReactiveFormsModule, MatCardModule, InputModule, MatButtonModule, MatProgressSpinnerModule,],
  templateUrl: './login.component.html',
  styleUrl: './login.component.scss',
})
export class LoginComponent {

  loading = false;

  loginForm = this.fb.group({
    email: ['', [Validators.email, Validators.required]],
    password: ['', [Validators.required]]
  });

  constructor(
    private fb: FormBuilder,
    private authService: AuthService,
    private router: Router,
  ) { }

  async login() {
    try {
      if (this.loginForm.invalid) {
        this.loginForm.markAllAsTouched();
        return
      };

      const { email, password } = this.loginForm.value;

      this.loading = true;
      this.loginForm.disable();

      await lastValueFrom(this.authService.login(email as string, password as string));

      this.router.navigate(['/']);

      this.loading = false;
    } catch (error) {
      this.loginForm.enable();
      this.loading = false;

    }
  }

}
