import { AfterViewInit, Component, DestroyRef, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, ReactiveFormsModule } from '@angular/forms';
import { CurrencyPipe } from '@angular/common';
import { Router, RouterLink } from '@angular/router';
import { InputModule } from '@components/input/input.module';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import { debounceTime, distinctUntilChanged, lastValueFrom } from 'rxjs';
import { TableFilter } from '@models/table';
import { Employee } from '@models/employee';
import { EmployeeService } from '@services/employee.service';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatTableDataSource, MatTableModule } from '@angular/material/table';
import { MatPaginator, MatPaginatorModule, PageEvent } from '@angular/material/paginator';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatSort, MatSortModule, Sort } from '@angular/material/sort';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { ConfirmationDialogService } from '@services/confirmation-dialog.service';

@Component({
  selector: 'lk-employee-list',
  standalone: true,
  imports: [
    MatButtonModule, MatInputModule, MatFormFieldModule, MatIconModule, MatTableModule,
    MatPaginatorModule, MatSortModule, MatProgressSpinnerModule, MatTooltipModule,
    CurrencyPipe, ReactiveFormsModule, RouterLink, InputModule,
  ],
  templateUrl: './employee-list.component.html',
  styleUrl: './employee-list.component.scss'
})
export class EmployeeListComponent implements OnInit, AfterViewInit {
  @ViewChild(MatPaginator) paginator?: MatPaginator;
  @ViewChild(MatSort) sort?: MatSort;

  loading = false;
  displayedColumns: string[] = ['id', 'username', 'email', 'group', 'basicSalary', 'action'];
  dataSource = new MatTableDataSource<any>();

  // #region filter
  tableFilter: TableFilter = this.employeeService.filter;
  // control name in search form should be same with the name in column
  searchForm = this.fb.group({
    username: [''],
    email: ['']
  });

  constructor(
    private fb: FormBuilder,
    private employeeService: EmployeeService,
    private router: Router,
    private destroyRef: DestroyRef,
    private confirmationDialogService: ConfirmationDialogService,
  ) { }

  ngOnInit(): void {
    // override default filter logic
    this.dataSource.filterPredicate = (data: Employee, filter: string) => this.customFilterPredicate(data, filter);

    // continue with filter that updated last time, will be back to default if hard refresh page
    this.initFilterSearch();

    this.getDatas();

    this.searchForm.valueChanges.pipe(
      debounceTime(500),
      distinctUntilChanged(),
      takeUntilDestroyed(this.destroyRef),
    ).subscribe((data) => {
      this.setFilterSearch(data);
    });

  }

  ngAfterViewInit(): void {
    if (this.paginator) {
      this.dataSource.paginator = this.paginator;
    }
    if (this.sort) {
      this.dataSource.sort = this.sort;
    }
  }

  async getDatas() {
    try {
      this.loading = true;
      const data = await lastValueFrom(this.employeeService.fetchEmployees()) as Employee[];
      setTimeout(() => {
        this.refreshDataSource(false);
        this.setFilterSearch(this.searchForm.value);
        this.dataSource.data = data;
        this.loading = false;
      }, 300);
    } catch (error) {
      this.loading = false;

    }
  }

  // navigate to employee detail page
  goToDetail(row: Employee, event: any) {
    event.stopPropagation();
    if (row && row.id) {
      this.router.navigate([`/employee/${row.id}`]);
    }
  }
  // navigate to employee edit page
  goToEdit(row: Employee, event: any) {
    event.stopPropagation();
    if (row && row.id) {
      this.router.navigate([`/employee/${row.id}/edit`]);
    }
  }

  openDeleteDialog(row: Employee, event: any) {
    event.stopPropagation();
    const dialogRef = this.confirmationDialogService.deleteEmployee(row.email);

    dialogRef.afterClosed().pipe(takeUntilDestroyed(this.destroyRef)).subscribe(res => {
      if (res && row.id) {
        this.deleteEmployee(row.id);
      }
    });

  }

  async deleteEmployee(id: number) {
    try {
      this.loading = true;
      await lastValueFrom(this.employeeService.deleteEmployee(id));
      this.getDatas();
    } catch (error) {
      this.loading = false;

    }
  }


  sortData(sort: Sort) {
    this.tableFilter.sortActive = sort.active;
    this.tableFilter.sortDirection = sort.direction;
  }

  pageOnChange(page: PageEvent) {
    this.tableFilter.pageIndex = page.pageIndex;
    this.tableFilter.pageSize = page.pageSize;
  }

  initFilterSearch() {
    this.searchForm.patchValue({ ...JSON.parse(this.employeeService.filter.search) });
    this.refreshDataSource();
  }

  setFilterSearch(data: any) {
    // just to handle typescript warning error
    const temp = data as { [name: string]: string };
    // trim object value; override temp object
    Object.keys(temp).forEach((o: string) => temp[o] = temp[o]?.trim());
    const parseData = JSON.stringify(temp);

    this.tableFilter.search = parseData;
    this.dataSource.filter = parseData;
  }

  reset() {
    this.employeeService.resetFilter();
    this.tableFilter = this.employeeService.filter;
    this.searchForm.reset();
    this.refreshDataSource();
  }

  refreshDataSource(isSkipData = true) {
    if (this.dataSource.paginator) {
      this.dataSource.paginator.pageIndex = this.tableFilter.pageIndex;
      this.dataSource.paginator.pageSize = this.tableFilter.pageSize;

    }
    if (isSkipData) {
      this.dataSource.data = this.dataSource.data;
    }
  }

  customFilterPredicate = (data: any, filter: string) => {
    const parseFilter: { [name: string]: string } = JSON.parse(filter);
    // store every filter match with data
    const rules: boolean[] = [];
    for (const key in parseFilter) {
      if (parseFilter[key] && data[key] !== undefined) {
        rules.push(data[key].toLowerCase().includes(parseFilter[key].toLocaleLowerCase()));
      }
    }
    // use 'every' for logic AND
    return rules.every(v => v === true);
  }
}
