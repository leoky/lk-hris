import { CurrencyPipe, DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatDividerModule } from '@angular/material/divider';
import { MatIconModule } from '@angular/material/icon';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatTooltipModule } from '@angular/material/tooltip';
import { ActivatedRoute, Router } from '@angular/router';
import { CardModule } from '@components/card/card.module';
import { ToolbarSecondaryComponent } from '@components/toolbar-secondary/toolbar-secondary.component';
import { Employee } from '@models/employee';
import { EmployeeService } from '@services/employee.service';
import { lastValueFrom } from 'rxjs';

@Component({
  selector: 'lk-employee-detail',
  standalone: true,
  imports: [
    MatDividerModule, MatProgressSpinnerModule, MatIconModule, MatButtonModule, MatTooltipModule,
    CardModule, CurrencyPipe, DatePipe, ToolbarSecondaryComponent,
  ],
  templateUrl: './employee-detail.component.html',
  styleUrl: './employee-detail.component.scss'
})
export class EmployeeDetailComponent implements OnInit {

  loading = false;

  employee: Employee | null = null;

  constructor(
    private employeeService: EmployeeService,
    private route: ActivatedRoute,
    private router: Router,
  ) { }

  ngOnInit(): void {
    const employeeId = this.route.snapshot.paramMap.get('id');
    if (employeeId) {
      this.getDatas(Number(employeeId));
    }
  }

  async getDatas(id: number) {
    try {
      this.loading = true;
      const data = await lastValueFrom(this.employeeService.fetchEmployeeDetail(id)) as Employee || null;
      if (data) {
        this.employee = data;
      } else {
        this.back();
      }
      this.loading = false;
    } catch (error) {
      this.loading = false;

    }
  }

  async deleteEmployee(event: any) {
    try {
      event.stopPropagation();
      this.loading = true;
      if (this.employee && this.employee.id) {
        const res = await this.employeeService.deleteEmployeeWithConfirmation(this.employee.id, this.employee.email);
        if (res) {
          this.back();
        }
      }
      this.loading = false;
    } catch (error) {
      this.loading = false;

    }
  }

  back() {
    this.router.navigate(['/employees']);
  }
}
