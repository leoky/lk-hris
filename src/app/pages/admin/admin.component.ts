import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Component } from '@angular/core';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import { MatSidenavModule } from '@angular/material/sidenav';
import { RouterOutlet } from '@angular/router';
import { SidenavComponent } from '@components/sidenav/sidenav.component';
import { ToolbarComponent } from '@components/toolbar/toolbar.component';
@Component({
  selector: 'lk-admin',
  standalone: true,
  imports: [RouterOutlet, MatSidenavModule, SidenavComponent, ToolbarComponent],
  templateUrl: './admin.component.html',
  styleUrl: './admin.component.scss'
})
export class AdminComponent {

  isMobile = false;

  constructor(
    breakpointObserver: BreakpointObserver
  ) {
    breakpointObserver.observe([Breakpoints.Handset]).pipe(takeUntilDestroyed()).subscribe(result => {
      this.isMobile = result.matches;
    });
  }

}
