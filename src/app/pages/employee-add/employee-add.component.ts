import { AsyncPipe, DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, ReactiveFormsModule, Validators } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatDividerModule } from '@angular/material/divider';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { ActivatedRoute, Router } from '@angular/router';
import { InputModule } from '@components/input/input.module';
import { NumberOnlyDirective } from '@directives/validators/number-only/number-only.directive';
import { dummyGroup } from '../../../../public/dummy/group-dummy';
import { EmployeeService } from '@services/employee.service';
import { lastValueFrom } from 'rxjs';
import { Employee } from '@models/employee';
import { MatSnackBar } from '@angular/material/snack-bar';
import { CardModule } from '@components/card/card.module';
import { ToolbarSecondaryComponent } from '@components/toolbar-secondary/toolbar-secondary.component';
import { MatAutocompleteModule } from '@angular/material/autocomplete';

@Component({
  selector: 'lk-employee-add',
  standalone: true,
  imports: [
    MatInputModule, MatFormFieldModule, MatDividerModule, MatButtonModule, MatProgressSpinnerModule,
    InputModule, NumberOnlyDirective, CardModule, ToolbarSecondaryComponent, MatAutocompleteModule,
    ReactiveFormsModule, DatePipe, AsyncPipe,
  ],
  templateUrl: './employee-add.component.html',
  styleUrl: './employee-add.component.scss'
})
export class EmployeeAddComponent implements OnInit {

  employeeForm = this.fb.group({
    username: ['', [Validators.required]],
    firstName: ['', [Validators.required]],
    lastName: ['', [Validators.required]],
    email: ['', [Validators.required, Validators.email]],
    birthDate: ['', [Validators.required]],
    basicSalary: ['', [Validators.required, Validators.min(1000000)]],
    group: ['', [Validators.required]],
    description: ['', [Validators.required]],
    status: ['', [Validators.required]],
  });

  loading = false;
  minBirthDate = new Date();
  listGroup = dummyGroup;
  filteredGroups = dummyGroup;

  // #region edit Mode
  employeeId: number | null = null; // null mean add mode

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private employeeService: EmployeeService,
    private snackBarService: MatSnackBar,
    private route: ActivatedRoute,
  ) { }

  ngOnInit(): void {
    const id = this.route.snapshot.paramMap.get('id');
    if (id) {
      this.employeeId = Number(id);
      this.getEmployeeDetail(this.employeeId);
    }

  }

  filterGroup(event: any) {
    const value = event.target.value;
    this.filteredGroups = this.listGroup.filter(g => g.value.toLocaleLowerCase().includes(value));
  }

  async getEmployeeDetail(id: number) {
    try {
      this.loading = true;
      const result = await lastValueFrom(this.employeeService.fetchEmployeeDetail(id)) as Employee;
      this.employeeForm.patchValue({
        username: result.username,
        firstName: result.firstName,
        lastName: result.lastName,
        email: result.email,
        birthDate: this.convertToDateInput(result.birthDate),
        basicSalary: result.basicSalary + '',
        group: result.group.toLocaleLowerCase(),
        description: result.description,
        status: result.status,
      });
      this.loading = false;
    } catch (error) {
      this.loading = false;
    }
  }

  async submit() {
    try {
      if (this.employeeForm.invalid) {
        this.employeeForm.markAllAsTouched();
        this.snackBarService.open('Form is incomplete', 'close');
        return;
      };
      this.loading = true;
      this.employeeForm.disable();

      const body: Employee = {
        username: this.employeeForm.value.username || '',
        firstName: this.employeeForm.value.firstName || '',
        lastName: this.employeeForm.value.lastName || '',
        email: this.employeeForm.value.email || '',
        birthDate: this.convertToDateInput(new Date(this.employeeForm.value.birthDate!)),
        basicSalary: Number(this.employeeForm.value.basicSalary!),
        status: this.employeeForm.value.status || '',
        group: this.employeeForm.value.group || '',
        description: this.employeeForm.value.description || '',
      }

      if (this.employeeId) {
        // update
        await lastValueFrom(this.employeeService.updateEmployee(body, this.employeeId));
        this.snackBarService.open('Employee has been updated', 'close');
      } else {
        // add 
        await lastValueFrom(this.employeeService.addEmployee(body));
        this.snackBarService.open('1 employee has been added', 'close');
      }

      this.loading = false;
      this.router.navigate(['/employees']);

    } catch (error) {
      this.employeeForm.enable();
      this.loading = false;

    }
  }

  cancel() {
    this.router.navigate(['/employees']);
  }

  convertToDateInput = (value: Date | string) => {
    const newDate = new Date(value);
    let month: string | number = newDate.getMonth() + 1;
    month = month >= 10 ? month : `0${month}`;
    let date: string | number = newDate.getDate();
    date = date >= 10 ? date : `0${date}`;
    return `${newDate.getFullYear()}-${month}-${date}`;
  }

}
