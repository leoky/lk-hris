import { inject } from '@angular/core';
import { CanMatchFn, Route, Router, UrlSegment } from '@angular/router';
import { AuthService } from '@services/auth.service';
import { map, take } from 'rxjs';

const customGuard = (route: Route, segments: UrlSegment[]) => {
  const authService = inject(AuthService);
  const router = inject(Router);

  return authService.isLogin$.pipe(
    take(1),
    map(isLogin => {
      if (isLogin) {
        router.navigate(['/']);
      }
      return !isLogin;
    })
  );
}

export const loginGuardCanMatch: CanMatchFn = (route, segments) => {
  return customGuard(route, segments);
};
