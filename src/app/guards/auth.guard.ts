import { inject } from '@angular/core';
import { CanMatchFn, Route, Router, UrlSegment } from '@angular/router';
import { AuthService } from '@services/auth.service';
import { take, tap } from 'rxjs';

const customGuard = (route: Route, segments: UrlSegment[]) => {
  const authService = inject(AuthService);
  const router = inject(Router);

  return authService.isLogin$.pipe(
    take(1),
    tap(isLogin => {
      if (isLogin === false) {
        router.navigate(['/login']);
      }
    })
  );
}

export const authGuardCanMatch: CanMatchFn = (route, segments) => {
  return customGuard(route, segments);
};
