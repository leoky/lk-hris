import { Routes } from '@angular/router';
import { authGuardCanMatch } from './guards/auth.guard';
import { loginGuardCanMatch } from './guards/login.guard';

export const routes: Routes = [
    {
        path: '',
        redirectTo: '/employees',
        pathMatch: 'full'
    },
    {
        path: 'login',
        loadComponent: () => import('./pages/login/login.component').then(c => c.LoginComponent),
        canMatch: [loginGuardCanMatch],
    },
    {
        path: '',
        loadComponent: () => import('./pages/admin/admin.component').then(c => c.AdminComponent),
        canMatch: [authGuardCanMatch],
        children: [
            {
                path: 'employees',
                loadComponent: () => import('./pages/employee-list/employee-list.component').then(c => c.EmployeeListComponent)
            },
            {
                path: 'employee/add',
                loadComponent: () => import('./pages/employee-add/employee-add.component').then(c => c.EmployeeAddComponent),
            },
            {
                path: 'employee/:id/edit',
                loadComponent: () => import('./pages/employee-add/employee-add.component').then(c => c.EmployeeAddComponent),
            },
            {
                path: 'employee/:id',
                loadComponent: () => import('./pages/employee-detail/employee-detail.component').then(c => c.EmployeeDetailComponent)
            },
        ]
    }
];
